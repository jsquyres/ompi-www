<?php
include_once("$topdir/includes/nav.inc");

$this_dir = "hwloc";
$this_nav[] = new Nav("Download v1.9 (beta)", "$topdir/software/hwloc/v1.9/");
$this_nav[] = new Nav("Download v1.8 (stable)", "$topdir/software/hwloc/v1.8/");
$this_nav[] = new Nav("Download v1.7 (old)", "$topdir/software/hwloc/v1.7/");
$this_nav[] = new Nav("Download v1.6 (older)", "$topdir/software/hwloc/v1.6/");
$this_nav[] = new Nav("Download v1.5 (older)", "$topdir/software/hwloc/v1.5/");
$this_nav[] = new Nav("Download v1.4 (ancient)", "$topdir/software/hwloc/v1.4/");
$this_nav[] = new Nav("Download v1.3 (ancient)", "$topdir/software/hwloc/v1.3/");
$this_nav[] = new Nav("Download v1.2 (ancient)", "$topdir/software/hwloc/v1.2/");
$this_nav[] = new Nav("Download v1.1 (ancient)", "$topdir/software/hwloc/v1.1/");
$this_nav[] = new Nav("Download v1.0 (jurassic)", "$topdir/software/hwloc/v1.0/");
$this_nav[] = new Nav("Download v0.9 (big-bang)", "$topdir/software/hwloc/v0.9/");
$this_nav[] = new Nav("Documentation", "$topdir/projects/hwloc/doc/");
$this_nav[] = new Nav("Tutorials", "$topdir/projects/hwloc/tutorials/");
$this_nav[] = new Nav("License", "$topdir/projects/hwloc/license.php");
$this_nav[] = new Nav("Mailing lists", "$topdir/community/lists/hwloc.php");
$this_nav[] = new Nav("Bug tracking", "https://git.open-mpi.org/trac/hwloc/report");
$this_nav[] = new Nav("Git access", "$topdir/projects/hwloc/git.php");
$this_nav[] = new Nav("Nightly snapshots", "$topdir/software/hwloc/nightly/");
