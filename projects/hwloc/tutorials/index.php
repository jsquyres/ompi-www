<?php
$topdir = "../../..";
$title = "Portable Hardware Locality (hwloc) Tutorials";
include_once("$topdir/software/hwloc/current/version.inc");
include_once("$topdir/projects/hwloc/nav.inc");
include_once("$topdir/includes/header.inc");
include_once("$topdir/includes/code.inc");

?>

<p>The entire material (slides, exercices and solutions) of several hwloc tutorials is available from here.</p>

<ul>
<li>
 2013/12/05 &mdash; 1 hour Introduction (University of San Luis, Argentina)
 &mdash; <a href="20131205-UNSL-hwloc-tutorial.pdf">Slides</a>
</li>
<li>
 2013/01/21 &mdash; 3-4 hours Tutorial (HiPEAC conference, Berlin, Germany)
 &mdash; <a href="20130121-HiPEAC-hwloc-tutorial.html">Details</a>
 &mdash; <a href="20130121-HiPEAC-hwloc-tutorial.pdf">Slides</a>
 &mdash; <a href="20130121-HiPEAC-hwloc-tutorial.tar.bz2">Material</a>
</li>
<li>
 2013/01/15 &mdash; 3-4 hours Tutorial (ComPAS French conference, Grenoble, France)
 &mdash; <a href="20130115-ComPAS-hwloc-tutorial.html">Details (in French)</a>
 &mdash; <a href="20130115-ComPAS-hwloc-tutorial.pdf">Slides (in English)</a>
 &mdash; <a href="20130115-ComPAS-hwloc-tutorial.tar.bz2">Material (in English)</a>
</li>
<li>
 2012/07/02 &mdash; 1 hour Tutorial (Porto Allegre, Brazil)
 &mdash; <a href="20120702-POA-hwloc-tutorial.pdf">Slides</a>
 &mdash; <a href="20120702-POA-hwloc-tutorial.html">Details and Material</a>
</li>
</ul>

<p>See also the <a href="<?php print "$topdir/projects/hwloc/doc" ?>">documentation</a> page.</p>

<?php
include_once("$topdir/includes/footer.inc");
?>
