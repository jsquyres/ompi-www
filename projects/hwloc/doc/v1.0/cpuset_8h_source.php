<?php
$topdir = "../../../..";
# Note that we must use the PHP "$$" indirection to assign to the
# "title" variable, because if we list "$ title" (without the space)
# in this file, Doxygen will replace it with a string title.
$ver = basename(getcwd());
$thwarting_doxygen_preprocessor = "title";
$$thwarting_doxygen_preprocessor = "Portable Hardware Locality (hwloc) Documentation: $ver";
$header_include_file = "$topdir/projects/hwloc/doc/$ver/www.open-mpi.org-css.inc";

include_once("$topdir/projects/hwloc/nav.inc");
include_once("$topdir/includes/header.inc");
include_once("$topdir/includes/code.inc");
?>
<!-- Generated by Doxygen 1.6.2 -->
<div class="navigation" id="top">
  <div class="tabs">
    <ul>
      <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
      <li><a href="pages.php"><span>Related&nbsp;Pages</span></a></li>
      <li><a href="modules.php"><span>Modules</span></a></li>
      <li><a href="annotated.php"><span>Data&nbsp;Structures</span></a></li>
      <li class="current"><a href="files.php"><span>Files</span></a></li>
    </ul>
  </div>
  <div class="tabs">
    <ul>
      <li><a href="files.php"><span>File&nbsp;List</span></a></li>
      <li><a href="globals.php"><span>Globals</span></a></li>
    </ul>
  </div>
<h1>cpuset.h</h1><a href="cpuset_8h.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">/*</span>
<a name="l00002"></a>00002 <span class="comment"> * Copyright © 2009 CNRS, INRIA, Université Bordeaux 1</span>
<a name="l00003"></a>00003 <span class="comment"> * Copyright © 2009-2010 Cisco Systems, Inc.  All rights reserved.</span>
<a name="l00004"></a>00004 <span class="comment"> * See COPYING in top-level directory.</span>
<a name="l00005"></a>00005 <span class="comment"> */</span>
<a name="l00006"></a>00006 
<a name="l00011"></a>00011 <span class="preprocessor">#ifndef HWLOC_CPUSET_H</span>
<a name="l00012"></a>00012 <span class="preprocessor"></span><span class="preprocessor">#define HWLOC_CPUSET_H</span>
<a name="l00013"></a>00013 <span class="preprocessor"></span>
<a name="l00014"></a>00014 <span class="preprocessor">#include &lt;hwloc/config.h&gt;</span>
<a name="l00015"></a>00015 
<a name="l00016"></a>00016 
<a name="l00030"></a><a class="code" href="group__hwlocality__cpuset.php#ga7366332f7090f5b54d4b25a0c2c4b411">00030</a> <span class="keyword">typedef</span> <span class="keyword">struct </span>hwloc_cpuset_s * <a class="code" href="group__hwlocality__cpuset.php#ga7366332f7090f5b54d4b25a0c2c4b411" title="Set of CPUs represented as an opaque pointer to an internal bitmask.">hwloc_cpuset_t</a>;
<a name="l00031"></a><a class="code" href="group__hwlocality__cpuset.php#gad2f7833583d020af31e01554251dbe11">00031</a> <span class="keyword">typedef</span> <span class="keyword">const</span> <span class="keyword">struct </span>hwloc_cpuset_s * <a class="code" href="group__hwlocality__cpuset.php#gad2f7833583d020af31e01554251dbe11">hwloc_const_cpuset_t</a>;
<a name="l00032"></a>00032 
<a name="l00033"></a>00033 
<a name="l00034"></a>00034 <span class="comment">/*</span>
<a name="l00035"></a>00035 <span class="comment"> * CPU set allocation, freeing and copying.</span>
<a name="l00036"></a>00036 <span class="comment"> */</span>
<a name="l00037"></a>00037 
<a name="l00044"></a>00044  <a class="code" href="group__hwlocality__cpuset.php#ga7366332f7090f5b54d4b25a0c2c4b411" title="Set of CPUs represented as an opaque pointer to an internal bitmask.">hwloc_cpuset_t</a> <a class="code" href="group__hwlocality__cpuset.php#gaf270165b6a08e8418fcfb68f5793ff7f" title="Allocate a new empty CPU set.">hwloc_cpuset_alloc</a>(<span class="keywordtype">void</span>) ;
<a name="l00045"></a>00045 
<a name="l00047"></a>00047  <span class="keywordtype">void</span> <a class="code" href="group__hwlocality__cpuset.php#gaaac6c1536cdcc35f1a1a3a9ab84da80d" title="Free CPU set set.">hwloc_cpuset_free</a>(<a class="code" href="group__hwlocality__cpuset.php#ga7366332f7090f5b54d4b25a0c2c4b411" title="Set of CPUs represented as an opaque pointer to an internal bitmask.">hwloc_cpuset_t</a> set);
<a name="l00048"></a>00048 
<a name="l00050"></a>00050  <a class="code" href="group__hwlocality__cpuset.php#ga7366332f7090f5b54d4b25a0c2c4b411" title="Set of CPUs represented as an opaque pointer to an internal bitmask.">hwloc_cpuset_t</a> <a class="code" href="group__hwlocality__cpuset.php#ga468c6e3fd92a9d0db1fb56634a851be3" title="Duplicate CPU set set by allocating a new CPU set and copying set&amp;#39;s contents...">hwloc_cpuset_dup</a>(<a class="code" href="group__hwlocality__cpuset.php#gad2f7833583d020af31e01554251dbe11">hwloc_const_cpuset_t</a> set) ;
<a name="l00051"></a>00051 
<a name="l00053"></a>00053  <span class="keywordtype">void</span> <a class="code" href="group__hwlocality__cpuset.php#ga27a3b6994bd6f20c1f26d10bdb29ac0b" title="Copy the contents of CPU set src into the already allocated CPU set dst.">hwloc_cpuset_copy</a>(<a class="code" href="group__hwlocality__cpuset.php#ga7366332f7090f5b54d4b25a0c2c4b411" title="Set of CPUs represented as an opaque pointer to an internal bitmask.">hwloc_cpuset_t</a> dst, <a class="code" href="group__hwlocality__cpuset.php#gad2f7833583d020af31e01554251dbe11">hwloc_const_cpuset_t</a> src);
<a name="l00054"></a>00054 
<a name="l00055"></a>00055 
<a name="l00056"></a>00056 <span class="comment">/*</span>
<a name="l00057"></a>00057 <span class="comment"> * Cpuset/String Conversion</span>
<a name="l00058"></a>00058 <span class="comment"> */</span>
<a name="l00059"></a>00059 
<a name="l00067"></a>00067  <span class="keywordtype">int</span> <a class="code" href="group__hwlocality__cpuset.php#ga4ed0a2badc6ff03f4d91a8d3c505b3e6" title="Stringify a cpuset.">hwloc_cpuset_snprintf</a>(<span class="keywordtype">char</span> * restrict buf, <span class="keywordtype">size_t</span> buflen, <a class="code" href="group__hwlocality__cpuset.php#gad2f7833583d020af31e01554251dbe11">hwloc_const_cpuset_t</a> set);
<a name="l00068"></a>00068 
<a name="l00074"></a>00074  <span class="keywordtype">int</span> <a class="code" href="group__hwlocality__cpuset.php#ga7a89398cbc58c9095aa094b9aeacbf00" title="Stringify a cpuset into a newly allocated string.">hwloc_cpuset_asprintf</a>(<span class="keywordtype">char</span> ** strp, <a class="code" href="group__hwlocality__cpuset.php#gad2f7833583d020af31e01554251dbe11">hwloc_const_cpuset_t</a> set);
<a name="l00075"></a>00075 
<a name="l00080"></a>00080  <span class="keywordtype">int</span> <a class="code" href="group__hwlocality__cpuset.php#gab6fb26149e25d4e5719a787ee01bacaa" title="Parse a cpuset string and stores it in CPU set set.">hwloc_cpuset_from_string</a>(<a class="code" href="group__hwlocality__cpuset.php#ga7366332f7090f5b54d4b25a0c2c4b411" title="Set of CPUs represented as an opaque pointer to an internal bitmask.">hwloc_cpuset_t</a> set, const <span class="keywordtype">char</span> * restrict <span class="keywordtype">string</span>);
<a name="l00081"></a>00081 
<a name="l00082"></a>00082 
<a name="l00083"></a>00083 <span class="comment">/*</span>
<a name="l00084"></a>00084 <span class="comment"> *  Primitives &amp; macros for building, modifying and consulting &quot;sets&quot; of cpus.</span>
<a name="l00085"></a>00085 <span class="comment"> */</span>
<a name="l00086"></a>00086 
<a name="l00088"></a>00088  <span class="keywordtype">void</span> <a class="code" href="group__hwlocality__cpuset.php#gacabf3491be3ab41b4ad1ee28f72db89e" title="Empty the CPU set set.">hwloc_cpuset_zero</a>(<a class="code" href="group__hwlocality__cpuset.php#ga7366332f7090f5b54d4b25a0c2c4b411" title="Set of CPUs represented as an opaque pointer to an internal bitmask.">hwloc_cpuset_t</a> set);
<a name="l00089"></a>00089 
<a name="l00091"></a>00091  <span class="keywordtype">void</span> <a class="code" href="group__hwlocality__cpuset.php#gacdc29003a0663e9b8b3a9d405a94fb70" title="Fill CPU set set with all possible CPUs (even if those CPUs don&amp;#39;t exist or are...">hwloc_cpuset_fill</a>(<a class="code" href="group__hwlocality__cpuset.php#ga7366332f7090f5b54d4b25a0c2c4b411" title="Set of CPUs represented as an opaque pointer to an internal bitmask.">hwloc_cpuset_t</a> set);
<a name="l00092"></a>00092 
<a name="l00094"></a>00094  <span class="keywordtype">void</span> <a class="code" href="group__hwlocality__cpuset.php#ga81218f1945e8fa25bbbc4e6277019122" title="Setup CPU set set from unsigned long mask.">hwloc_cpuset_from_ulong</a>(<a class="code" href="group__hwlocality__cpuset.php#ga7366332f7090f5b54d4b25a0c2c4b411" title="Set of CPUs represented as an opaque pointer to an internal bitmask.">hwloc_cpuset_t</a> set, <span class="keywordtype">unsigned</span> <span class="keywordtype">long</span> mask);
<a name="l00095"></a>00095 
<a name="l00097"></a>00097  <span class="keywordtype">void</span> <a class="code" href="group__hwlocality__cpuset.php#gac473267f1aa161c3e3e2a26ef25a477c" title="Setup CPU set set from unsigned long mask used as i -th subset.">hwloc_cpuset_from_ith_ulong</a>(<a class="code" href="group__hwlocality__cpuset.php#ga7366332f7090f5b54d4b25a0c2c4b411" title="Set of CPUs represented as an opaque pointer to an internal bitmask.">hwloc_cpuset_t</a> set, <span class="keywordtype">unsigned</span> i, <span class="keywordtype">unsigned</span> <span class="keywordtype">long</span> mask);
<a name="l00098"></a>00098 
<a name="l00100"></a>00100  <span class="keywordtype">unsigned</span> <span class="keywordtype">long</span> <a class="code" href="group__hwlocality__cpuset.php#ga5eb912bf1d0572127c3eed1b8a47e6ac" title="Convert the beginning part of CPU set set into unsigned long mask.">hwloc_cpuset_to_ulong</a>(<a class="code" href="group__hwlocality__cpuset.php#gad2f7833583d020af31e01554251dbe11">hwloc_const_cpuset_t</a> set) ;
<a name="l00101"></a>00101 
<a name="l00103"></a>00103  <span class="keywordtype">unsigned</span> <span class="keywordtype">long</span> <a class="code" href="group__hwlocality__cpuset.php#ga7576f6a70291feafe9e538942c8b7ee5" title="Convert the i -th subset of CPU set set into unsigned long mask.">hwloc_cpuset_to_ith_ulong</a>(<a class="code" href="group__hwlocality__cpuset.php#gad2f7833583d020af31e01554251dbe11">hwloc_const_cpuset_t</a> set, <span class="keywordtype">unsigned</span> i) ;
<a name="l00104"></a>00104 
<a name="l00106"></a>00106  <span class="keywordtype">void</span> <a class="code" href="group__hwlocality__cpuset.php#ga8ee7aa4827fb49af47eac9b66c74fd78" title="Empty the CPU set set and add CPU cpu.">hwloc_cpuset_cpu</a>(<a class="code" href="group__hwlocality__cpuset.php#ga7366332f7090f5b54d4b25a0c2c4b411" title="Set of CPUs represented as an opaque pointer to an internal bitmask.">hwloc_cpuset_t</a> set, <span class="keywordtype">unsigned</span> cpu);
<a name="l00107"></a>00107 
<a name="l00109"></a>00109  <span class="keywordtype">void</span> <a class="code" href="group__hwlocality__cpuset.php#ga02d86bba61be473bfdceb336c9087736" title="Empty the CPU set set and add all but the CPU cpu.">hwloc_cpuset_all_but_cpu</a>(<a class="code" href="group__hwlocality__cpuset.php#ga7366332f7090f5b54d4b25a0c2c4b411" title="Set of CPUs represented as an opaque pointer to an internal bitmask.">hwloc_cpuset_t</a> set, <span class="keywordtype">unsigned</span> cpu);
<a name="l00110"></a>00110 
<a name="l00112"></a>00112  <span class="keywordtype">void</span> <a class="code" href="group__hwlocality__cpuset.php#ga793d0c31b524337355ddce1c6568a866" title="Add CPU cpu in CPU set set.">hwloc_cpuset_set</a>(<a class="code" href="group__hwlocality__cpuset.php#ga7366332f7090f5b54d4b25a0c2c4b411" title="Set of CPUs represented as an opaque pointer to an internal bitmask.">hwloc_cpuset_t</a> set, <span class="keywordtype">unsigned</span> cpu);
<a name="l00113"></a>00113 
<a name="l00115"></a>00115  <span class="keywordtype">void</span> <a class="code" href="group__hwlocality__cpuset.php#ga2f15dc90a98d14db8022f76a38c39727" title="Add CPUs from begincpu to endcpu in CPU set set.">hwloc_cpuset_set_range</a>(<a class="code" href="group__hwlocality__cpuset.php#ga7366332f7090f5b54d4b25a0c2c4b411" title="Set of CPUs represented as an opaque pointer to an internal bitmask.">hwloc_cpuset_t</a> set, <span class="keywordtype">unsigned</span> begincpu, <span class="keywordtype">unsigned</span> endcpu);
<a name="l00116"></a>00116 
<a name="l00118"></a>00118  <span class="keywordtype">void</span> <a class="code" href="group__hwlocality__cpuset.php#ga59f2a65f5260581ee642b0a8375be564" title="Remove CPU cpu from CPU set set.">hwloc_cpuset_clr</a>(<a class="code" href="group__hwlocality__cpuset.php#ga7366332f7090f5b54d4b25a0c2c4b411" title="Set of CPUs represented as an opaque pointer to an internal bitmask.">hwloc_cpuset_t</a> set, <span class="keywordtype">unsigned</span> cpu);
<a name="l00119"></a>00119 
<a name="l00121"></a>00121  <span class="keywordtype">void</span> <a class="code" href="group__hwlocality__cpuset.php#ga214d599ff5b66073460b7ee9a75016a8" title="Remove CPUs from begincpu to endcpu in CPU set set.">hwloc_cpuset_clr_range</a>(<a class="code" href="group__hwlocality__cpuset.php#ga7366332f7090f5b54d4b25a0c2c4b411" title="Set of CPUs represented as an opaque pointer to an internal bitmask.">hwloc_cpuset_t</a> set, <span class="keywordtype">unsigned</span> begincpu, <span class="keywordtype">unsigned</span> endcpu);
<a name="l00122"></a>00122 
<a name="l00124"></a>00124  <span class="keywordtype">int</span> <a class="code" href="group__hwlocality__cpuset.php#ga7236a9cf8be3ded29a912790e35065f7" title="Test whether CPU cpu is part of set set.">hwloc_cpuset_isset</a>(<a class="code" href="group__hwlocality__cpuset.php#gad2f7833583d020af31e01554251dbe11">hwloc_const_cpuset_t</a> set, <span class="keywordtype">unsigned</span> cpu) ;
<a name="l00125"></a>00125 
<a name="l00127"></a>00127  <span class="keywordtype">int</span> <a class="code" href="group__hwlocality__cpuset.php#gac5b8ad0c32e9d14c587eabde188182a9" title="Test whether set set is empty.">hwloc_cpuset_iszero</a>(<a class="code" href="group__hwlocality__cpuset.php#gad2f7833583d020af31e01554251dbe11">hwloc_const_cpuset_t</a> set) ;
<a name="l00128"></a>00128 
<a name="l00130"></a>00130  <span class="keywordtype">int</span> <a class="code" href="group__hwlocality__cpuset.php#gab8703a0f28053bd3981852548e3182d1" title="Test whether set set is completely full.">hwloc_cpuset_isfull</a>(<a class="code" href="group__hwlocality__cpuset.php#gad2f7833583d020af31e01554251dbe11">hwloc_const_cpuset_t</a> set) ;
<a name="l00131"></a>00131 
<a name="l00133"></a>00133  <span class="keywordtype">int</span> <a class="code" href="group__hwlocality__cpuset.php#ga9534d84820beade1e6155a1e734307a2" title="Test whether set set1 is equal to set set2.">hwloc_cpuset_isequal</a> (<a class="code" href="group__hwlocality__cpuset.php#gad2f7833583d020af31e01554251dbe11">hwloc_const_cpuset_t</a> set1, <a class="code" href="group__hwlocality__cpuset.php#gad2f7833583d020af31e01554251dbe11">hwloc_const_cpuset_t</a> set2) ;
<a name="l00134"></a>00134 
<a name="l00136"></a>00136  <span class="keywordtype">int</span> <a class="code" href="group__hwlocality__cpuset.php#gad7cbab558a9a80652c3ad0b30d488f04" title="Test whether sets set1 and set2 intersects.">hwloc_cpuset_intersects</a> (<a class="code" href="group__hwlocality__cpuset.php#gad2f7833583d020af31e01554251dbe11">hwloc_const_cpuset_t</a> set1, <a class="code" href="group__hwlocality__cpuset.php#gad2f7833583d020af31e01554251dbe11">hwloc_const_cpuset_t</a> set2) ;
<a name="l00137"></a>00137 
<a name="l00139"></a>00139  <span class="keywordtype">int</span> <a class="code" href="group__hwlocality__cpuset.php#ga135bbe4177fbfe8b14bcbe6aad765801" title="Test whether set sub_set is part of set super_set.">hwloc_cpuset_isincluded</a> (<a class="code" href="group__hwlocality__cpuset.php#gad2f7833583d020af31e01554251dbe11">hwloc_const_cpuset_t</a> sub_set, <a class="code" href="group__hwlocality__cpuset.php#gad2f7833583d020af31e01554251dbe11">hwloc_const_cpuset_t</a> super_set) ;
<a name="l00140"></a>00140 
<a name="l00142"></a>00142  <span class="keywordtype">void</span> <a class="code" href="group__hwlocality__cpuset.php#ga9654f87331e6f33090bed3d326346e85" title="Or sets set1 and set2 and store the result in set res.">hwloc_cpuset_or</a> (<a class="code" href="group__hwlocality__cpuset.php#ga7366332f7090f5b54d4b25a0c2c4b411" title="Set of CPUs represented as an opaque pointer to an internal bitmask.">hwloc_cpuset_t</a> res, <a class="code" href="group__hwlocality__cpuset.php#gad2f7833583d020af31e01554251dbe11">hwloc_const_cpuset_t</a> set1, <a class="code" href="group__hwlocality__cpuset.php#gad2f7833583d020af31e01554251dbe11">hwloc_const_cpuset_t</a> set2);
<a name="l00143"></a>00143 
<a name="l00145"></a>00145  <span class="keywordtype">void</span> <a class="code" href="group__hwlocality__cpuset.php#gacd5a399d475b7d75e71489177650b6df" title="And sets set1 and set2 and store the result in set res.">hwloc_cpuset_and</a> (<a class="code" href="group__hwlocality__cpuset.php#ga7366332f7090f5b54d4b25a0c2c4b411" title="Set of CPUs represented as an opaque pointer to an internal bitmask.">hwloc_cpuset_t</a> res, <a class="code" href="group__hwlocality__cpuset.php#gad2f7833583d020af31e01554251dbe11">hwloc_const_cpuset_t</a> set1, <a class="code" href="group__hwlocality__cpuset.php#gad2f7833583d020af31e01554251dbe11">hwloc_const_cpuset_t</a> set2);
<a name="l00146"></a>00146 
<a name="l00148"></a>00148  <span class="keywordtype">void</span> <a class="code" href="group__hwlocality__cpuset.php#ga338fa981505cb2c87e3e9dc543a698b9" title="And set set1 and the negation of set2 and store the result in set res.">hwloc_cpuset_andnot</a> (<a class="code" href="group__hwlocality__cpuset.php#ga7366332f7090f5b54d4b25a0c2c4b411" title="Set of CPUs represented as an opaque pointer to an internal bitmask.">hwloc_cpuset_t</a> res, <a class="code" href="group__hwlocality__cpuset.php#gad2f7833583d020af31e01554251dbe11">hwloc_const_cpuset_t</a> set1, <a class="code" href="group__hwlocality__cpuset.php#gad2f7833583d020af31e01554251dbe11">hwloc_const_cpuset_t</a> set2);
<a name="l00149"></a>00149 
<a name="l00151"></a>00151  <span class="keywordtype">void</span> <a class="code" href="group__hwlocality__cpuset.php#ga82f8b81aa98c3a488e21838620da8852" title="Xor sets set1 and set2 and store the result in set res.">hwloc_cpuset_xor</a> (<a class="code" href="group__hwlocality__cpuset.php#ga7366332f7090f5b54d4b25a0c2c4b411" title="Set of CPUs represented as an opaque pointer to an internal bitmask.">hwloc_cpuset_t</a> res, <a class="code" href="group__hwlocality__cpuset.php#gad2f7833583d020af31e01554251dbe11">hwloc_const_cpuset_t</a> set1, <a class="code" href="group__hwlocality__cpuset.php#gad2f7833583d020af31e01554251dbe11">hwloc_const_cpuset_t</a> set2);
<a name="l00152"></a>00152 
<a name="l00154"></a>00154  <span class="keywordtype">void</span> <a class="code" href="group__hwlocality__cpuset.php#ga9494650a8cb93e1dc77590e2393519a5" title="Negate set set and store the result in set res.">hwloc_cpuset_not</a> (<a class="code" href="group__hwlocality__cpuset.php#ga7366332f7090f5b54d4b25a0c2c4b411" title="Set of CPUs represented as an opaque pointer to an internal bitmask.">hwloc_cpuset_t</a> res, <a class="code" href="group__hwlocality__cpuset.php#gad2f7833583d020af31e01554251dbe11">hwloc_const_cpuset_t</a> set);
<a name="l00155"></a>00155 
<a name="l00160"></a>00160  <span class="keywordtype">int</span> <a class="code" href="group__hwlocality__cpuset.php#gaec614784aab4c1bd4d279fc548f4aa40" title="Compute the first CPU (least significant bit) in CPU set set.">hwloc_cpuset_first</a>(<a class="code" href="group__hwlocality__cpuset.php#gad2f7833583d020af31e01554251dbe11">hwloc_const_cpuset_t</a> set) ;
<a name="l00161"></a>00161 
<a name="l00166"></a>00166  <span class="keywordtype">int</span> <a class="code" href="group__hwlocality__cpuset.php#gaf4fb6d1ca812633f2e5eaa8ae98b1aef" title="Compute the last CPU (most significant bit) in CPU set set.">hwloc_cpuset_last</a>(<a class="code" href="group__hwlocality__cpuset.php#gad2f7833583d020af31e01554251dbe11">hwloc_const_cpuset_t</a> set) ;
<a name="l00167"></a>00167 
<a name="l00172"></a>00172  <span class="keywordtype">int</span> <a class="code" href="group__hwlocality__cpuset.php#ga6bf3fd9ea7b0d0fcb5656bab8b68c1bf" title="Compute the next CPU in CPU set set which is after CPU prev_cpu.">hwloc_cpuset_next</a>(<a class="code" href="group__hwlocality__cpuset.php#gad2f7833583d020af31e01554251dbe11">hwloc_const_cpuset_t</a> set, <span class="keywordtype">unsigned</span> prev_cpu) ;
<a name="l00173"></a>00173 
<a name="l00180"></a>00180  <span class="keywordtype">void</span> <a class="code" href="group__hwlocality__cpuset.php#gace7ad3d2a71d9884e7a28311228931af" title="Keep a single CPU among those set in CPU set set.">hwloc_cpuset_singlify</a>(<a class="code" href="group__hwlocality__cpuset.php#ga7366332f7090f5b54d4b25a0c2c4b411" title="Set of CPUs represented as an opaque pointer to an internal bitmask.">hwloc_cpuset_t</a> set);
<a name="l00181"></a>00181 
<a name="l00187"></a>00187  <span class="keywordtype">int</span> <a class="code" href="group__hwlocality__cpuset.php#ga6d54e9fa190351368ea08d02b6b09d32" title="Compare CPU sets set1 and set2 using their lowest index CPU.">hwloc_cpuset_compare_first</a>(<a class="code" href="group__hwlocality__cpuset.php#gad2f7833583d020af31e01554251dbe11">hwloc_const_cpuset_t</a> set1, <a class="code" href="group__hwlocality__cpuset.php#gad2f7833583d020af31e01554251dbe11">hwloc_const_cpuset_t</a> set2) ;
<a name="l00188"></a>00188 
<a name="l00194"></a>00194  <span class="keywordtype">int</span> <a class="code" href="group__hwlocality__cpuset.php#gad3ec83a8f86764d87676a7a48c837d70" title="Compare CPU sets set1 and set2 using their highest index CPU.">hwloc_cpuset_compare</a>(<a class="code" href="group__hwlocality__cpuset.php#gad2f7833583d020af31e01554251dbe11">hwloc_const_cpuset_t</a> set1, <a class="code" href="group__hwlocality__cpuset.php#gad2f7833583d020af31e01554251dbe11">hwloc_const_cpuset_t</a> set2) ;
<a name="l00195"></a>00195 
<a name="l00201"></a>00201  <span class="keywordtype">int</span> <a class="code" href="group__hwlocality__cpuset.php#ga432291e25ca6e91ab689b08cdc26d3fa" title="Compute the &amp;quot;weight&amp;quot; of CPU set set (i.e., number of CPUs that are in the...">hwloc_cpuset_weight</a>(<a class="code" href="group__hwlocality__cpuset.php#gad2f7833583d020af31e01554251dbe11">hwloc_const_cpuset_t</a> set) ;
<a name="l00202"></a>00202 
<a name="l00212"></a><a class="code" href="group__hwlocality__cpuset.php#ga8f896ce703ad1740fdf9ce8ac6361359">00212</a> <span class="preprocessor">#define hwloc_cpuset_foreach_begin(cpu, set) \</span>
<a name="l00213"></a>00213 <span class="preprocessor">do { \</span>
<a name="l00214"></a>00214 <span class="preprocessor">        for (cpu = hwloc_cpuset_first(set); \</span>
<a name="l00215"></a>00215 <span class="preprocessor">             (unsigned) cpu != (unsigned) -1; \</span>
<a name="l00216"></a>00216 <span class="preprocessor">             cpu = hwloc_cpuset_next(set, cpu)) { \</span>
<a name="l00217"></a>00217 <span class="preprocessor"></span>
<a name="l00221"></a><a class="code" href="group__hwlocality__cpuset.php#gae2974be78a7d7cddbd38cb23fcc6240a">00221</a> <span class="preprocessor">#define hwloc_cpuset_foreach_end() \</span>
<a name="l00222"></a>00222 <span class="preprocessor">        } \</span>
<a name="l00223"></a>00223 <span class="preprocessor">} while (0)</span>
<a name="l00224"></a>00224 <span class="preprocessor"></span>
<a name="l00227"></a>00227 <span class="preprocessor">#endif </span><span class="comment">/* HWLOC_CPUSET_H */</span>
</pre></div></div>
<?php
include_once("$topdir/includes/footer.inc");
