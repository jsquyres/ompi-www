<?php
$topdir = "../../../..";
# Note that we must use the PHP "$$" indirection to assign to the
# "title" variable, because if we list "$ title" (without the space)
# in this file, Doxygen will replace it with a string title.
$ver = basename(getcwd());
$thwarting_doxygen_preprocessor = "title";
$$thwarting_doxygen_preprocessor = "Portable Hardware Locality (hwloc) Documentation: $ver";
$header_include_file = "$topdir/projects/hwloc/doc/$ver/www.open-mpi.org-css.inc";

include_once("$topdir/projects/hwloc/nav.inc");
include_once("$topdir/includes/header.inc");
include_once("$topdir/includes/code.inc");
?>
<!-- Generated by Doxygen 1.7.4 -->
  <div id="navrow1" class="tabs">
    <ul class="tablist">
      <li><a href="index.php"><span>Main&#160;Page</span></a></li>
      <li><a href="pages.php"><span>Related&#160;Pages</span></a></li>
      <li><a href="modules.php"><span>Modules</span></a></li>
      <li class="current"><a href="annotated.php"><span>Data&#160;Structures</span></a></li>
      <li><a href="files.php"><span>Files</span></a></li>
    </ul>
  </div>
  <div id="navrow2" class="tabs2">
    <ul class="tablist">
      <li><a href="annotated.php"><span>Data&#160;Structures</span></a></li>
      <li><a href="functions.php"><span>Data&#160;Fields</span></a></li>
    </ul>
  </div>
  <div id="nav-path" class="navpath">
    <ul>
      <li class="navelem"><a class="el" href="a00016.php">hwloc_obj_attr_u</a>      </li>
      <li class="navelem"><a class="el" href="a00011.php">hwloc_bridge_attr_s</a>      </li>
    </ul>
  </div>
</div>
<div class="header">
  <div class="summary">
<a href="#pub-attribs">Data Fields</a>  </div>
  <div class="headertitle">
<div class="title">hwloc_obj_attr_u::hwloc_bridge_attr_s Struct Reference</div>  </div>
</div>
<div class="contents">
<!-- doxytag: class="hwloc_obj_attr_u::hwloc_bridge_attr_s" -->
<p>Bridge specific Object Attribues.  
 <a href="a00011.php#details">More...</a></p>

<p><code>#include &lt;<a class="el" href="a00032_source.php">hwloc.h</a>&gt;</code></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-attribs"></a>
Data Fields</h2></td></tr>
<tr><td class="memItemLeft" >union {</td></tr>
<tr><td class="memItemLeft" >&#160;&#160;&#160;struct <a class="el" href="a00021.php">hwloc_pcidev_attr_s</a>&#160;&#160;&#160;<a class="el" href="a00011.php#ab5c564e7c95b747dae9eb84ec0a2c31e">pci</a></td></tr>
<tr><td class="memItemLeft" valign="top">}&#160;</td><td class="memItemRight" valign="bottom"><a class="el" href="a00011.php#a00ce9d99fc8792d1044fe25dc58605fe">upstream</a></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="a00040.php#ga0a947e8c5adcc729b126bd09c01a0153">hwloc_obj_bridge_type_t</a>&#160;</td><td class="memItemRight" valign="bottom"><a class="el" href="a00011.php#a265dd2164aa2df4972e25a029da72125">upstream_type</a></td></tr>
<tr><td class="memItemLeft" >union {</td></tr>
<tr><td class="memItemLeft" >&#160;&#160;&#160;struct {</td></tr>
<tr><td class="memItemLeft" >&#160;&#160;&#160;&#160;&#160;&#160;unsigned short&#160;&#160;&#160;<a class="el" href="a00011.php#a2c31e565a5f0d23d0a0a3dd3ec8f4b17">domain</a></td></tr>
<tr><td class="memItemLeft" >&#160;&#160;&#160;&#160;&#160;&#160;unsigned char&#160;&#160;&#160;<a class="el" href="a00011.php#ae2d9dd73ef1d32045c584a8e66d2f83f">secondary_bus</a></td></tr>
<tr><td class="memItemLeft" >&#160;&#160;&#160;&#160;&#160;&#160;unsigned char&#160;&#160;&#160;<a class="el" href="a00011.php#af3f3f7d76bf03e8d2afa721c2b8d6771">subordinate_bus</a></td></tr>
<tr><td class="memItemLeft" valign="top">&#160;&#160;&#160;}&#160;&#160;&#160;<a class="el" href="a00011.php#a5a20be20e09d811d141b6332ff864706">pci</a></td></tr>
<tr><td class="memItemLeft" valign="top">}&#160;</td><td class="memItemRight" valign="bottom"><a class="el" href="a00011.php#acaf1ae02e37182bbb6966f8c4f35e499">downstream</a></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="a00040.php#ga0a947e8c5adcc729b126bd09c01a0153">hwloc_obj_bridge_type_t</a>&#160;</td><td class="memItemRight" valign="bottom"><a class="el" href="a00011.php#ac6a169b672d0e9f75756fd5665828b93">downstream_type</a></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">unsigned&#160;</td><td class="memItemRight" valign="bottom"><a class="el" href="a00011.php#a336c8b22893d5d734d8c9dfca4066b46">depth</a></td></tr>
</table>
<hr/><a name="details" id="details"></a><h2>Detailed Description</h2>
<div class="textblock"><p>Bridge specific Object Attribues. </p>
</div><hr/><h2>Field Documentation</h2>
<a class="anchor" id="a336c8b22893d5d734d8c9dfca4066b46"></a><!-- doxytag: member="hwloc_obj_attr_u::hwloc_bridge_attr_s::depth" ref="a336c8b22893d5d734d8c9dfca4066b46" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">unsigned <a class="el" href="a00011.php#a336c8b22893d5d734d8c9dfca4066b46">hwloc_obj_attr_u::hwloc_bridge_attr_s::depth</a></td>
        </tr>
      </table>
</div>
<div class="memdoc">

</div>
</div>
<a class="anchor" id="a2c31e565a5f0d23d0a0a3dd3ec8f4b17"></a><!-- doxytag: member="hwloc_obj_attr_u::hwloc_bridge_attr_s::domain" ref="a2c31e565a5f0d23d0a0a3dd3ec8f4b17" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">unsigned short <a class="el" href="a00011.php#a2c31e565a5f0d23d0a0a3dd3ec8f4b17">hwloc_obj_attr_u::hwloc_bridge_attr_s::domain</a></td>
        </tr>
      </table>
</div>
<div class="memdoc">

</div>
</div>
<a class="anchor" id="acaf1ae02e37182bbb6966f8c4f35e499"></a><!-- doxytag: member="hwloc_obj_attr_u::hwloc_bridge_attr_s::downstream" ref="acaf1ae02e37182bbb6966f8c4f35e499" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">union { ... }   <a class="el" href="a00011.php#acaf1ae02e37182bbb6966f8c4f35e499">hwloc_obj_attr_u::hwloc_bridge_attr_s::downstream</a></td>
        </tr>
      </table>
</div>
<div class="memdoc">

</div>
</div>
<a class="anchor" id="ac6a169b672d0e9f75756fd5665828b93"></a><!-- doxytag: member="hwloc_obj_attr_u::hwloc_bridge_attr_s::downstream_type" ref="ac6a169b672d0e9f75756fd5665828b93" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="a00040.php#ga0a947e8c5adcc729b126bd09c01a0153">hwloc_obj_bridge_type_t</a> <a class="el" href="a00011.php#ac6a169b672d0e9f75756fd5665828b93">hwloc_obj_attr_u::hwloc_bridge_attr_s::downstream_type</a></td>
        </tr>
      </table>
</div>
<div class="memdoc">

</div>
</div>
<a class="anchor" id="ab5c564e7c95b747dae9eb84ec0a2c31e"></a><!-- doxytag: member="hwloc_obj_attr_u::hwloc_bridge_attr_s::pci" ref="ab5c564e7c95b747dae9eb84ec0a2c31e" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">struct <a class="el" href="a00021.php">hwloc_pcidev_attr_s</a> <a class="el" href="a00011.php#ab5c564e7c95b747dae9eb84ec0a2c31e">hwloc_obj_attr_u::hwloc_bridge_attr_s::pci</a></td>
        </tr>
      </table>
</div>
<div class="memdoc">

</div>
</div>
<a class="anchor" id="a5a20be20e09d811d141b6332ff864706"></a><!-- doxytag: member="hwloc_obj_attr_u::hwloc_bridge_attr_s::pci" ref="a5a20be20e09d811d141b6332ff864706" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">struct { ... }   <a class="el" href="a00011.php#ab5c564e7c95b747dae9eb84ec0a2c31e">hwloc_obj_attr_u::hwloc_bridge_attr_s::pci</a></td>
        </tr>
      </table>
</div>
<div class="memdoc">

</div>
</div>
<a class="anchor" id="ae2d9dd73ef1d32045c584a8e66d2f83f"></a><!-- doxytag: member="hwloc_obj_attr_u::hwloc_bridge_attr_s::secondary_bus" ref="ae2d9dd73ef1d32045c584a8e66d2f83f" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">unsigned char <a class="el" href="a00011.php#ae2d9dd73ef1d32045c584a8e66d2f83f">hwloc_obj_attr_u::hwloc_bridge_attr_s::secondary_bus</a></td>
        </tr>
      </table>
</div>
<div class="memdoc">

</div>
</div>
<a class="anchor" id="af3f3f7d76bf03e8d2afa721c2b8d6771"></a><!-- doxytag: member="hwloc_obj_attr_u::hwloc_bridge_attr_s::subordinate_bus" ref="af3f3f7d76bf03e8d2afa721c2b8d6771" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">unsigned char <a class="el" href="a00011.php#af3f3f7d76bf03e8d2afa721c2b8d6771">hwloc_obj_attr_u::hwloc_bridge_attr_s::subordinate_bus</a></td>
        </tr>
      </table>
</div>
<div class="memdoc">

</div>
</div>
<a class="anchor" id="a00ce9d99fc8792d1044fe25dc58605fe"></a><!-- doxytag: member="hwloc_obj_attr_u::hwloc_bridge_attr_s::upstream" ref="a00ce9d99fc8792d1044fe25dc58605fe" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">union { ... }   <a class="el" href="a00011.php#a00ce9d99fc8792d1044fe25dc58605fe">hwloc_obj_attr_u::hwloc_bridge_attr_s::upstream</a></td>
        </tr>
      </table>
</div>
<div class="memdoc">

</div>
</div>
<a class="anchor" id="a265dd2164aa2df4972e25a029da72125"></a><!-- doxytag: member="hwloc_obj_attr_u::hwloc_bridge_attr_s::upstream_type" ref="a265dd2164aa2df4972e25a029da72125" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="a00040.php#ga0a947e8c5adcc729b126bd09c01a0153">hwloc_obj_bridge_type_t</a> <a class="el" href="a00011.php#a265dd2164aa2df4972e25a029da72125">hwloc_obj_attr_u::hwloc_bridge_attr_s::upstream_type</a></td>
        </tr>
      </table>
</div>
<div class="memdoc">

</div>
</div>
<hr/>The documentation for this struct was generated from the following file:<ul>
<li><a class="el" href="a00032_source.php">hwloc.h</a></li>
</ul>
</div>
<?php
include_once("$topdir/includes/footer.inc");
