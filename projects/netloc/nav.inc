<?php
include_once("$topdir/includes/nav.inc");

$this_dir = "netloc";
$this_nav[] = new Nav("Download v0.5 (beta)", "$topdir/software/netloc/v0.5/");
$this_nav[] = new Nav("FAQ", "$topdir/projects/netloc/faq/");
$this_nav[] = new Nav("License", "$topdir/projects/netloc/license.php");
$this_nav[] = new Nav("Mailing lists", "$topdir/community/lists/netloc.php");
$this_nav[] = new Nav("Bug tracking", "https://git.open-mpi.org/trac/netloc");
$this_nav[] = new Nav("Git access", "$topdir/projects/netloc/git.php");
$this_nav[] = new Nav("Nightly snapshots", "$topdir/software/netloc/nightly/");
