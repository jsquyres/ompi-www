<?php
include_once("$topdir/includes/nav.inc");

$this_dir = "otpo";
$this_nav[] = new Nav("Download v1.0 (stable)", "$topdir/software/otpo/v1.0/");
$this_nav[] = new Nav("Documentation", "$topdir/projects/otpo/doc/");
$this_nav[] = new Nav("License", "$topdir/projects/otpo/license.php");
$this_nav[] = new Nav("Mailing lists", "$topdir/community/lists/otpo.php");
$this_nav[] = new Nav("Bug tracking", "https://svn.open-mpi.org/trac/otpo/report");
$this_nav[] = new Nav("Subversion checkout", "$topdir/projects/otpo/svn.php");
