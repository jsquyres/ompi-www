<?php
  $topdir = "../../..";
  $title = "PMI Exascale (pmix): Nightly snapshot tarballs";
  include_once("$topdir/projects/pmix/nav.inc");
  include_once("$topdir/includes/header.inc");
?>

The following versions are available as nightly snapshots:

<p>
<ul>

<p> <strong>Current development</strong>

<li><a href="master/"><strong>Master</strong></a> (development head):
These snapshots are from the svn master branch and reflect the current
head of development.  The usual disclaimers about the state of
development code apply.</li>

</ul>
</p>

<?php 
  include_once("$topdir/includes/footer.inc"); 
