<?php
// Assume that $base has already been set

$this_nav[] = new Nav("OMPI announce archives", "$base/community/lists/announce/");
$this_nav[] = new Nav("OMPI users archives", "$base/community/lists/users/");
$this_nav[] = new Nav("OMPI devel archives", "$base/community/lists/devel/");
$this_nav[] = new Nav("OMPI docs archives", "$base/community/lists/docs/");
