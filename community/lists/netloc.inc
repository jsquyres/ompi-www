<?php
// Assume that $base has already been set

$this_nav[] = new Nav("netloc announce archives", "$base/community/lists/netloc-announce/");
$this_nav[] = new Nav("netloc users archives", "$base/community/lists/netloc-users/");
$this_nav[] = new Nav("netloc devel archives", "$base/community/lists/netloc-devel/");
