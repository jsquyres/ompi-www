<?php
// Assume that $base has already been set

$this_nav[] = new Nav("hwloc announce archives", "$base/community/lists/hwloc-announce/");
$this_nav[] = new Nav("hwloc users archives", "$base/community/lists/hwloc-users/");
$this_nav[] = new Nav("hwloc devel archives", "$base/community/lists/hwloc-devel/");
