<?php
// Assume that $base has already been set

$this_nav[] = new Nav("MTT announce archives", "$base/community/lists/mtt-announce/");
$this_nav[] = new Nav("MTT users archives", "$base/community/lists/mtt-users/");
$this_nav[] = new Nav("MTT developers archives", "$base/community/lists/mtt-devel/");
