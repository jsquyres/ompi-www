<?php
// Assume that $base has already been set

$this_nav[] = new Nav("PMIx announce archives", "$base/community/lists/pmix-announce/");
$this_nav[] = new Nav("PMIx users archives", "$base/community/lists/pmix-users/");
$this_nav[] = new Nav("PMIx devel archives", "$base/community/lists/pmix-devel/");
