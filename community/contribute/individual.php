<?php
  $topdir = "../..";
  $title = "Contributing to Open MPI as an Individual";

  include_once("nav.inc");
  include_once("$topdir/includes/header.inc");
?>

<p><em> Please also see the FAQ under the catrgory "<a href="<?php
print($topdir); ?>/faq/?category=contributing">Contributing to Open
MPI</a>" for more information.</em>

<p> If you are an individual who is responsible for all of your own
intellectual property, you need to fill out an Open MPI Individual
Contributor License Agreement form.  It is available in two formats:

<ul>

<li> <?php
print("<a href=\"open-mpi-individual-contributor-agrement.doc\">");
?>Microsoft Word</a></li>

<li> <?php
print("<a href=\"open-mpi-individual-contributor-agrement.pdf\">");
?>PDF</a></li>

</ul>

Instructions are provided on the form how to submit it to the Open MPI
project.

<?php 
  include_once("$topdir/includes/footer.inc"); 
