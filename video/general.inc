<?php

$titles[] = "What is MPI?  What is Open MPI?";

$a = "what-is-mpi";
$anchors[] = $a;

$durations[$a] = "20:18";
$video_heights[$a] = 480;
$video_widths[$a] = 640;

$base = "what-is-[open]-mpi";
$videos[$a] = "$base.mov";
$notes[$a] = $quicktime;
$extras[$a][] = "$base-1up.pdf";
$extras[$a][] = "$base-2up.pdf";

$summaries[$a] = "Brief description of what MPI is, including a brief 
summary of the Open MPI Project.";

##########################################################################