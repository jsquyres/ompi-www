<?php

$titles[] = "Slurm Workload Manager";

$a = "SchedMD_MorrisJette";
$anchors[] = $a;

$durations[$a] = "1:42:18";
$video_heights[$a] = 720;
$video_widths[$a] = 1280;

$base = "SchedMD_MorrisJette";
$notes[$a] = "This screencast assumes that you already have $quicktime.";
$videos[$a] = "$base.mov";
$extras[$a][] = "Slurm_EMC_Dec2012.pdf";

$summaries[$a] = "Architecture, configuration, and use of Slurm - intended for developers.";
