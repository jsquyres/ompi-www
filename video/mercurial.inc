<?php

$titles[] = "Using SVN+Mercurial for temporary development";

$a = "svn-and-mercurial";
$anchors[] = $a;

$durations[$a] = "9:54";
$video_heights[$a] = 480;
$video_widths[$a] = 640;

$base = "svn-and-hg";
$notes[$a] = "This screencast assumes that you already have basic Mercurial knowledge; $quicktime";
$videos[$a] = "$base.mov";

$summaries[$a] = "Basic overview of how to use Mercurial in conjunction with SVN for Open MPI development.";

##########################################################################