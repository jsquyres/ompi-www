<?php

$titles[] = "OpenFabrics Concepts";

$a = "openfabrics-concepts";
$anchors[] = $a;

$durations[$a] = "9:24";
$video_heights[$a] = 480;
$video_widths[$a] = 640;

$base = "openfabrics-concepts";
$videos[$a] = "$base.mov";
$notes[$a] = $quicktime;
$extras[$a][] = "$base-1up.pdf";
$extras[$a][] = "$base-2up.pdf";

$summaries[$a] = "Provides an overview of OpenFabrics concepts that
are relevant to Open MPI.";

##########################################################################


$titles[] = "Open MPI OpenFabrics Protocols (v1.2 series)";

$a = "ompi-openfabrics-protocols-v1.2";
$anchors[] = $a;

$durations[$a] = "22:29";
$video_heights[$a] = 480;
$video_widths[$a] = 640;

$base = "ompi-openfabrics-protocols-v1.2";
$videos[$a] = "$base.mov";
$notes[$a] = $quicktime;
$extras[$a][] = "$base-1up.pdf";
$extras[$a][] = "$base-2up.pdf";

$summaries[$a] = "Describes the protocols that Open MPI uses on
OpenFabrics networks.  

It is suggested that you watch the \"OpenFabrics Concepts\" screencast
before this one.";

##########################################################################

$titles[] = "Tuning the Openib BTL (v1.2 series)";

$a = "openib-tuning-v1.2";
$anchors[] = $a;

$durations[$a] = "21:33";
$video_heights[$a] = 480;
$video_widths[$a] = 640;

$base = "openib-btl-tuning-v1.2";
$videos[$a] = "$base.mov";
$notes[$a] = $quicktime;
$extras[$a][] = "$base-1up.pdf";
$extras[$a][] = "$base-2up.pdf";

$summaries[$a] = "Describes how to tune the [openib] BTL (i.e., the Open
MPI plugin that provides point-to-point networking support for
OpenFabrics networks).  

It is suggested that you watch the \"OpenFabrics Concepts\" and \"Open
MPI OpenFabrics Protocols (v1.2 series)\" screencasts before this one.";

##########################################################################

$titles[] = "Openib BTL v1.3 Sneak Peak";

$a = "openib-sneak-peek-v1.3";
$anchors[] = $a;

$durations[$a] = "14:02";
$video_heights[$a] = 480;
$video_widths[$a] = 640;

$base = "openib-sneak-peek-v1.3";
$videos[$a] = "$base.mov";
$notes[$a] = $quicktime;
$extras[$a][] = "$base-1up.pdf";
$extras[$a][] = "$base-2up.pdf";

$summaries[$a] = "A \"sneak peek\" of features in the upcoming Open
MPI v1.3 Openib BTL (OpenFabrics transport).  Note that nothing in
these slides are definite until v1.3 ships.";

##########################################################################
