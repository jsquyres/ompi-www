<?php

$titles[] = "MR+";

$a = "Greenplum_RalphCastain";
$anchors[] = $a;

$durations[$a] = "1:02:40";
$video_heights[$a] = 720;
$video_widths[$a] = 1280;

$base = "Greenplum_RalphCastain";
$notes[$a] = "This screencast assumes that you already have $quicktime.";
$videos[$a] = "$base.mov";
$extras[$a][] = "$base-1up.pdf";
$extras[$a][] = "$base-2up.pdf";

$summaries[$a] = "Overview of Greenplum\'s port of Apache Hadoop\'s Map-Reduce system to Open MPI";
