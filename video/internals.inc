<?php

$titles[] = "Open MPI: Overview/Architecture";

$a = "Cisco_JeffSquyres";
$anchors[] = $a;

$durations[$a] = "2:09:04";
$video_heights[$a] = 720;
$video_widths[$a] = 1280;

$base = "Cisco_JeffSquyres";
$notes[$a] = "This screencast assumes that you already have $quicktime. Also, a change was made to OMPI's library dependencies shortly after the vide was created - details of the linking hierarchy on slide 91 have been revised. Please see the slides in the accompanying pdf files for the new linking chart";
$videos[$a] = "$base.mov";
$extras[$a][] = "$base-1up.pdf";
$extras[$a][] = "$base-2up.pdf";

$summaries[$a] = "Basic overview of Open MPI's architecture and general code base.";

##########################################################################

$titles[] = "Open MPI Data Transfer";

$a = "Sandia_BrianBarrett";
$anchors[] = $a;

$durations[$a] = "2:09:33";
$video_heights[$a] = 720;
$video_widths[$a] = 1280;

$base = "Sandia_BrianBarrett";
$notes[$a] = "This screencast assumes that you already have $quicktime";
$videos[$a] = "$base.mov";
$extras[$a][] = "$base-1up.pdf";
$extras[$a][] = "$base-2up.pdf";

$summaries[$a] = "Detailed overview of the Open MPI data transfer system.";

##########################################################################

$titles[] = "Scalable and Modular Parallel I/O for Open MPI";

$a = "Parallel_EdgarGabriel";
$anchors[] = $a;

$durations[$a] = "1:37:45";
$video_heights[$a] = 720;
$video_widths[$a] = 1280;

$base = "Parallel_EdgarGabriel";
$notes[$a] = "This screencast assumes that you already have $quicktime";
$videos[$a] = "$base.mov";
$extras[$a][] = "$base-1up.pdf";
$extras[$a][] = "$base-2up.pdf";

$summaries[$a] = "Overview of parallel I/O in Open MPI.";

##########################################################################

