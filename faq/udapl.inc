<?php

$q[] = "What is different between Sun Microsystems ClusterTools 7 and Open
MPI in regards to the uDAPL BTL?";

$anchor[] = "udapl";

$a[] = "Sun's ClusterTools is based off of Open MPI with one significant
difference: Sun's ClusterTools includes uDAPL RDMA capabilities in the
uDAPL BTL. Open MPI v1.2 uDAPL BTL does not include the RDMA
capabilities. These improvements do exist today in the Open MPI trunk
and will be included in future Open MPI releases.";

/////////////////////////////////////////////////////////////////////////

# Sample:

#$q[] = "How do I know what MCA parameters are available for tuning MPI performance?";

#$anchor[] = "ib-params";

#$a[] = "The [ompi_info] command can display all the parameters
#available for the [openib] BTL component:

#<faqcode>
## Show the openib BTL parameters
#shell$ ompi_info <font color=red><strong>--param btl openib</strong></font>
#</faqcode>";

/////////////////////////////////////////////////////////////////////////

$q[] = "What values are expected to be used by the btl_udapl_if_include and btl_udapl_if_exclude mca parameter?";

$anchor[] = "include-exclude";

$a[] = "The uDAPL BTL looks for a match from the uDAPL static registry which is contained in the dat.conf file. Each non commented or blank line is considered an interface. The first field of each interface entry is the value which must be supplied to the mca parameter in question.

Solaris Example:

<faqcode>
shell% datadm -v
ibd0  u1.2  nonthreadsafe  default  udapl_tavor.so.1  SUNW.1.0  \" \"  \"driver_name=tavor\"
shell% mpirun --mca btl_udapl_if_include ibd0 ...
</faqcode>

Linux Example:

<faqcode>
shell% cat /etc/dat.conf
OpenIB-cma u1.2 nonthreadsafe default /usr/local/ofed/lib64/libdaplcma.so dapl.1.2 \"ib0 0\" \"\"
OpenIB-bond u1.2 nonthreadsafe default /usr/local/ofed/lib64/libdaplcma.so dapl.1.2 \"bond0 0\" \"\"
shell% mpirun --mca btl_udapl_if_exclude OpenIB-bond ...
</faqcode>";

/////////////////////////////////////////////////////////////////////////

$q[] = "Where is the static uDAPL Registry found?";

$anchor[] = "udapl-registry";

$a[] = "Solaris: [/etc/dat/dat.conf]

Linux: [/etc/dat.conf]
";

/////////////////////////////////////////////////////////////////////////

$q[] = "How come the value reported by \"ifconfig\" is not accepted by the btl_udapl_if_include/btl_udapl_if_exclude MCA parameter?";

$anchor[] = "ifconfig-interface";

$a[] = "uDAPL queries a static registry defined in the dat.conf file to find available interfaces which can be used. As such, the uDAPL BTL needs to match the names found in the registry and these may differ from what is reported by \"ifconfig\".";

/////////////////////////////////////////////////////////////////////////

$q[] = "I get a warning message about not being able to register memory and possibly out of privileged memory while running on Solaris, what can I do?";

$anchor[] = "privileged-memory";

$a[] = "The error message probably looks something like this:

<faqcode>
WARNING: The uDAPL BTL is not able to register memory. Possibly out of
allowed privileged memory (i.e. memory that can be pinned). Increasing
the allowed privileged memory may alleviate this issue.
</faqcode>

One thing to do is increase the amount of available privileged
memory. On Solaris your system adminstrator can increase the amount of
available privileged memory by editing the [/etc/project] file on the
nodes. For more information see Solaris \"project\" man page.

<faqcode>
shell% man project
</faqcode>

As an example of increasing the privileged memory first determine the
amount available (example of typical value is 978MB):

<faqcode>
shell% prctl -n project.max-device-locked-memory -i project default
NAME    PRIVILEGE       VALUE    FLAG   ACTION          RECIPIENT
project.max-device-locked-memory
        privileged       978MB      -   deny            -
        system          16.0EB    max   deny            -
</faqcode>

To increase the amount of privileged memory edit [/etc/project] file:

Default [/etc/project] file.

<faqcode>
system:0::::
user.root:1::::
noproject:2::::
default:3::::
group.staff:10::::
</faqcode>

Change to, for example 4GB.

<faqcode>
system:0::::
user.root:1::::
noproject:2::::
default:3::::project.max-device-locked-memory=(priv, 4294967296, deny) 
group.staff:10::::
</faqcode>
";
