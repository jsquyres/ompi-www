<?php
include_once("$topdir/software/ompi/current/version.inc");

$q[] = "v1.7 Series";

$anchor[] = "v1.7";

$a[] = "

<ol>
<li>
<a
href=\"https://svn.open-mpi.org/trac/ompi/wiki/KnownIssues1.7\">1.7.3</a>
</li>
</ol>";

/////////////////////////////////////////////////////////////////////////
